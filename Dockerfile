FROM ruby:2.6.5-alpine

RUN apk update && apk upgrade && \
    apk --no-cache add build-base

WORKDIR /app

COPY Gemfile .

RUN gem install bundler && \
    bundle install --jobs 20 --retry 5

COPY . .

ENTRYPOINT ["rspec"]
  