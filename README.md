# AES GCM

## What does it do?

It encrypts/decrypts via [AES GCM](https://csrc.nist.gov/publications/detail/sp/800-38d/final).

## Why did you make this library?

Because it's a good choice for encrypting cookies and outside of Rails (which uses GCM via ActiveSupport) that's not so easy. This can also be used outside of cookies because, unlike the ActiveSupport library, it doesn't throw away the AAD (see below).

## How do I use it?

    require "aes-gcm/cbor"

    # The key/secret *must be in base 64 strict!*
    # Try not to lose it ;-)
    secret = SecureRandom.base64(16) # 128 / 8

    use Rack::Session::Cookie, {
      :key          =>  'rack.session',
      :domain       =>  'example.com',
      :httponly     =>  true,
      :path         =>  '/',
      :expire_after =>  2592000, # one month
      :coder        =>  AES_GCM::AES_128_GCM.new( key: secret ),
      :let_coder_handle_secure_encoding  => true,
    }

## The serializer

I want this to be as fast as possible so I chose [CBOR](https://cbor.io/) for the message packing.

An alternative:

    require 'aes-gcm'
    require 'msgpack'

    AES_GCM::PACKER = MessagePack

Any library that shares the API of CBOR/MessagePack can be dropped in.


## I want my Additional Authenticated Data data back!

Additional Authenticated Data (AAD) is something you want to transmit but can't keep secret, like headers.

> For example, within a network protocol, the AAD might include addresses, ports, sequence numbers, protocol version numbers, and other fields that indicate how the plaintext should be treated. It should not contain messages to be authenticated.

(From page 8 of the NIST article)

Cookies don't want the AAD (or Rack [doesn't care](https://github.com/rack/rack/blob/2.0.7/lib/rack/session/cookie.rb#L135)) so the `decode` method throws it away. If you want it then use the `_decode` method instead, e.g.

    secret  = SecureRandom.base64( key_length )
    aad     = Base64.strict_encode64 "Something Something"
    coder   = AES_GCM::AES_128_GCM.new( key: secret, auth_data: aad )
    msg     = coder.encode "Hello, World"

    # Later that day...
    coder = AES_GCM::AES_128_GCM.new( key: secret, auth_data: aad )
    plain,aad = coder._decode(msg)

It may come in handy.

## May I contribute?

That would be great! Just pop in a pull request (please think about tests if adding features!) or open an issue. All help/criticisms/questions will be gratefully received.

# Licence

See the LICENCE file.



