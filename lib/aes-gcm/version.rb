module AES_GCM
  # Library version, follows semver
  # @see https://semver.org/spec/v2.0.0.html
  VERSION = "0.0.1"
end