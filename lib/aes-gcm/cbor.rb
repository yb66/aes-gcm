require 'cbor'

module AES_GCM
  # The serializer.
  PACKER = CBOR
end

require_relative "../aes-gcm.rb"
