module AES_GCM

  # Methods for encoding and decoding a message using AES GCM.
  module GCMable
    # If this changes I only need to change it here
    # TODO think about making this pluggable

    # Because I'm lazy and it makes the expression too long to read,
    # these wrapper methods exist.

    # Encode to base 64 strict
    # @param [String] thing
    def en64 thing
      Base64.strict_encode64 thing
    end


    # Decode base 64 strict
    # @param [String] thing
    def de64 thing
      Base64.strict_decode64 thing
    end


    # String together the IV, encrypted message, and auth tag.
    # No need to use delimiters because the iv and tag sizes are known.
    # @param [String] iv The IV bytes.
    # @param [String] tag The auth_tag bytes.
    # @param [String] encrypted The encrypted message bytes.
    def concatenate iv, tag, encrypted
      # It's not insecure to send the IV, nor the tag.
      [iv, tag, encrypted].join.then{|x| en64 x }
    end


    # @param [String] incoming The IV, auth tag, and encrypted message, concatenated.
    # @return [Array<String,String,String>] iv,tag,message
    def split incoming
      interim = de64(incoming)
      [ interim.byteslice(0,@iv_size_bytes),
        interim.byteslice(@iv_size_bytes,@tag_size_bytes),
        interim.byteslice(@iv_size_bytes + @tag_size_bytes .. -1) 
      ]
    end


    # Encrypt a message.
    # @param [Object] message
    # @return [String] The IV, encrypted message, and auth tag, concatenated.
    def encode(message)
      return message if message.nil? or message.empty?
      _encode message
    end


    # Encryption, wrapped with error tagging and a #reset to protect against
    # reuse of the IV.
    # @api private
    # @see #encode
    def _encode(message)
      # Don't change the order of @cipher's calls, the order is important!
      @cipher.encrypt   # set mode
      @cipher.key       = @key
      # Don't put iv in an instance variable, only use once!
      iv  = @cipher.iv  = @cipher.random_iv
      [message, @auth_data]
        .map  {|data| PACKER.pack data }
        .then {|message,auth_data|
          @cipher.auth_data = auth_data
          @cipher.update(message) + @cipher.final
        }
        .then {|enc_message| concatenate iv, @cipher.auth_tag, enc_message }
    rescue => e
      # Tag all errors so they can be caught further up easily
      e.extend AES_GCM::Error
      raise
    ensure
      @cipher.reset # mainly to protect against IV reuse
    end


    # Decrypt a message.
    # @param [String] incoming Base64'd string.
    # @return [Object] The original data.
    def decode incoming
      original,_ = _decode incoming
      original
    end


    # Decryption, wrapped with error tagging and a #reset to protect against
    # reuse of the IV.
    # @api private
    # @param (see #decode)
    # @return [Object,String] The original object/data and the AAD.
    def _decode(incoming)
      return if @cipher.nil? or incoming.nil?
      iv,tag,message    = split incoming
      # Don't change the order of cipher's calls, the order is important!
      @cipher.decrypt   # set mode
      @cipher.key       = @key
      @cipher.iv        = iv
      @cipher.auth_tag  = tag
      @cipher.auth_data = PACKER.pack @auth_data # same as for encrypting
      (@cipher.update(message) + @cipher.final).then {|message|
        [ PACKER.unpack(message), @auth_data ]
      }
    rescue => e
      # Tag all errors so they can be caught further up easily
      e.extend AES_GCM::Error
      raise
    ensure
      @cipher.reset
    end
  end

end