require 'openssl'
require 'base64'
require_relative "aes-gcm/gcmable.rb"

# Encode and decode a message using AES GCM
# @see https://csrc.nist.gov/publications/detail/sp/800-38d/final
# @example
#   secret = SecureRandom.base64( 128.div 8 ) }
#   msg = AES_GCM::AES_128_GCM.new( key: secret ).encode('Hello, World') }
#   plain = AES_GCM::AES_128_GCM.new( key: secret ).decode(msg)}
module AES_GCM

  # For tagging all errors emanating within this library.
  module Error; end


  # Base class for AES-xxx-GCM
  # I know this (AES-xxx-GCM) isn't conventional naming but each part is important
  # so I want to make it clearer to read.
  class Base
    include GCMable


    # @param [String] auth_data Additional authenticated data (AAD). It's for something you want to transmit but can't keep secret, like headers. For example, within a network protocol, the AAD might include addresses, ports, sequence numbers, protocol version numbers, and other fields that indicate how the plaintext should be treated. It should not contain messages to be authenticated. Optional.
    # @param [String] key Byte 64 (strict) encoded string.
    def initialize key:, auth_data: ""
      @key            = Base64.strict_decode64 key
      @auth_data      = auth_data || ""
      @iv_size_bytes  = @cipher.iv_len
    end
  end


  # @example
  #   # The key/secret *must be in base 64 strict!*
  #   # Try not to lose it ;-)
  #   secret = SecureRandom.base64(16) # 128 / 8
  #
  #   use Rack::Session::Cookie, {
  #     :key          =>  'rack.session',
  #     :domain       =>  'example.com',
  #     :httponly     =>  true,
  #     :path         =>  '/',
  #     :expire_after =>  2592000, # one month
  #     :coder        =>  AES_GCM::AES_128_GCM.new( key: secret ),
  #     :let_coder_handle_secure_encoding  => true,
  #   }
  class AES_128_GCM < Base

    # @param [String] key 16 bytes.
    # @param (see AES_GCM#initialize)
    def initialize key:, auth_data: ""
      @bits           = 128
      @tag_size_bytes = 16 # 16 bytes
      @cipher = OpenSSL::Cipher::AES.new(@bits, :GCM)
      super
    end
  end


  # @example
  #   # The key/secret *must be in base 64 strict!*
  #   # Try not to lose it ;-)
  #   secret = SecureRandom.base64(32) # 256 / 8
  #
  #   use Rack::Session::Cookie, {
  #     :key          =>  'rack.session',
  #     :domain       =>  'example.com',
  #     :httponly     =>  true,
  #     :path         =>  '/',
  #     :expire_after =>  2592000, # one month
  #     :coder        =>  AES_GCM::AES_256_GCM.new( key: secret ),
  #     :let_coder_handle_secure_encoding  => true,
  #   }
  class AES_256_GCM < Base

    # @param [String] key 16 bytes.
    # @param (see AES_GCM#initialize)
    def initialize key:, auth_data: ""
      @bits           = 256
      @tag_size_bytes = 16 # 16 bytes
      @cipher = OpenSSL::Cipher::AES.new(@bits, :GCM)
      super
    end
  end
end