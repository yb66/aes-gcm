require 'spec_helper'
require 'aes-gcm/cbor'
require 'rack/test'
require 'securerandom'
if ENV["DEBUG"]
  require 'pry-byebug'
  binding.pry
end

class FilterLocalHost
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, body  = @app.call(env)
    headers.merge!( 'X-Custom-Header' => 'foo' )
    [status, headers, body]
  end
end

describe "When using with cookies" do
  include Rack::Test::Methods


  def app
    Rack::Builder.new do
      use Rack::Session::Cookie, {
        :key          =>  'rack.session',
        :domain       =>  'example.com',
        :httponly     =>  true,
        :path         =>  '/',
        :expire_after =>  2592000, # one month
        :coder        =>  AES_GCM::AES_256_GCM.new( key: SecureRandom.base64(32) ),
        :let_coder_handle_secure_encoding  => true,
      }
      map "/set" do
        run ->(env){
          puts "Entering /set" if ENV["DEBUG"]
          request = Rack::Request.new env
          count = (request.session["counter"] || 0).to_i
          puts "count: #{count}" if ENV["DEBUG"]
          count += 1
          puts "count: #{count}" if ENV["DEBUG"]
          request.session["counter"] = count
          response = Rack::Response.new {|response|
            response.write "I left the body!\n\n"
          }
        }
      end
      map '/' do
        run -> (env){
          puts "Entering /" if ENV["DEBUG"]
          request = Rack::Request.new env
          puts %Q!A visual sanity check:\n\t HTTP_COOKIE: #{env["HTTP_COOKIE"]}! if ENV["DEBUG"]
          count = request.session[:counter]
          response = Rack::Response.new {|response|
            response.write "count: #{count}\n\n"
          }
        }
      end
    end
  end

  context "checking the cookie" do
    context "One cycle" do
      context "Set the cookie and check it appears scrambled" do
        When { get("/set") }
        Then {
          last_response.headers["Set-Cookie"].start_with? "rack.session="
        }
        And {
          key,_,val = last_response.headers["Set-Cookie"].partition("=")
          fields = val.split ";"
          !fields.first.start_with? "count"
        }
      end
      
      context "A further request" do
        context "Was it unscrambled server side?" do
          When {
            get("/set")
            get "/", {}, {"HTTP_COOKIE" => last_response.headers["Set-Cookie"]}
          }
          Then { last_response.body == "count: 1\n\n" }
        end
      end
    end

    context "And again, set and check" do
      context "The setting" do
        When(:set_again) {
          get("/set")
          get "/set", {}, {"HTTP_COOKIE" => last_response.headers["Set-Cookie"]}
        }
        Then {
          last_response.headers["Set-Cookie"].start_with? "rack.session="
        }
        And {
          key,_,val = last_response.headers["Set-Cookie"].partition("=")
          fields = val.split ";"
          !fields.first.start_with? "count"
        }
      end
      
      context "Was it unscrambled and server side and updated?" do
        When(:count) {
          i = 0
          r = rand(2..10)
          get("/set") # i becomes 1
          while i < r do
            get "/set", {}, {"HTTP_COOKIE" => last_response.headers["Set-Cookie"]}
            i += 1
          end
          get "/", {}, {"HTTP_COOKIE" => last_response.headers["Set-Cookie"]}
          i += 1 # for the last get
        }
        Then { last_response.body == "count: #{count}\n\n" }
      end
    end
  end

end