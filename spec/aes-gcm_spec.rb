require 'spec_helper'
require "aes-gcm/cbor"
require 'securerandom'

module AES_GCM
[
  [AES_128_GCM,128],
  [AES_256_GCM,256],
].each do |(klass,bits)|
  describe "AES_GCM" do
      
    Then { AES_GCM::PACKER == CBOR }

    # I use separate instances of the (en/de)coder here, not because the same
    # instance couldn't do both but just to make certain that a fresh instance
    # would work just as well.

    Given(:key_length) { bits.div 8 }

    context 'An encrypted message is decryptable' do
      Given(:secret) { SecureRandom.base64( key_length ) }
      Given(:msg) { klass.new( key: secret ).encode('Hello, World') }
      Given(:plain) { klass.new( key: secret ).decode(msg)}
      Then { plain == 'Hello, World' }
    end


    context 'overly long key given' do
      Given(:secret) { SecureRandom.base64( key_length * 2 ) }
      Given(:msg) { klass.new( key: secret ).encode('Hello, World') }
      Given(:decoder) { klass.new( key: secret ) }
      Then {
        # The error initially raised is ArgumentError (by the OpenSSL lib)
        # but it's tagged with this library's error so it's easier to
        # find the source of the error. Either can be used to pass
        # this spec.
        expect { decoder.decode(msg) }.to raise_error AES_GCM::Error, "key must be #{key_length} bytes"
      }
      And {
        expect { decoder.decode(msg) }.to raise_error ArgumentError, "key must be #{key_length} bytes"
      }
    end

    context 'junk messages' do
      Given(:secret) { SecureRandom.base64( key_length ) }
      Given(:decoder) { klass.new( key: secret ) }
      Then {
        # The error initially raised is ArgumentError by the Base64 library
        # but it's tagged with this library's error so it's easier to
        # find the source of the error. Either can be used to pass
        # this spec.
        expect { decoder.decode('aaa--bbb-ccc') }.to raise_error AES_GCM::Error, "invalid base64"
      }
      And {
        expect { decoder.decode('aaa--bbb-ccc') }.to raise_error ArgumentError, "invalid base64"
      }
    end

    context 'tampered messages' do
      Given(:secret) { SecureRandom.base64( key_length ) }
      Given(:encoder) { klass.new( key: secret ) }
      Given(:original) { encoder.encode('Hello, World') }
      Given(:tampered_msg) {
        iv,tag,message = encoder.split original
        tampered_msg = encoder.concatenate iv, tag, message.reverse
      }
      Given(:decoder) { klass.new( key: secret ) }
      Then {
        # The error initially raised is OpenSSL::Cipher::CipherError
        # but it's tagged with this library's error so it's easier to
        # find the source of the error. Either can be used to pass
        # this spec.
        expect { decoder.decode tampered_msg }.to raise_error AES_GCM::Error 
      }
      And {
        expect { decoder.decode tampered_msg }.to raise_error OpenSSL::Cipher::CipherError
      }
    end


    context "Auth data" do
      # The Additional Authenticated Data (AAD) is something not to be tampered 
      # with but that can't be kept secret, 
      # like a recipient name (or something).
      # Base64 and a set length that's a multiple of 8 is probably best,
      # so here I use SecureRandom.base64 to make the test rigorous.


      context "Given none because it's optional" do
        Given(:secret) { SecureRandom.base64( key_length ) }
        Given(:msg) { klass.new( key: secret, auth_data: nil ).encode "Hello, World" }
        When(:plain) { klass.new( key: secret ).decode(msg)}
        Then { plain == "Hello, World" }
      end


      context "Given auth data that is valid" do
        context "Only retrieving the message" do
          Given(:secret) { SecureRandom.base64( key_length ) }
          Given(:aad) { SecureRandom.base64( key_length ) }
          Given(:msg) { klass.new( key: secret, auth_data: aad ).encode "Hello, World" }
          When(:plain) { klass.new( key: secret, auth_data: aad ).decode(msg)}
          Then { plain == "Hello, World" }
        end

        context "Retrieving the AAD too" do
          Given(:secret) { SecureRandom.base64( key_length ) }
          Given(:aad) { SecureRandom.base64( key_length ) }
          Given(:msg) { klass.new( key: secret, auth_data: aad ).encode "Hello, World" }
          When(:plain_and_aad) { klass.new( key: secret, auth_data: aad )._decode(msg)}
          Then {
            plain,_aad = *plain_and_aad
            plain == "Hello, World"
          }
          Then {
            plain,_aad = *plain_and_aad
            _aad == aad
          }
        end
      end

      context "Given auth data that is invalid" do
        Given(:secret) { SecureRandom.base64( key_length ) }
        Given(:aad) { SecureRandom.base64( key_length ) }
        Given(:bad_aad) { SecureRandom.base64( key_length ) }
        When(:msg) { klass.new( key: secret, auth_data: aad ).encode "Hello, World" }
        Then { 
          # Again, this is also an OpenSSL::Cipher::CipherError but it's
          # tagged to make debugging easier. I care about you, fellow programmer,
          # I really do.
          expect { klass.new( key: secret, auth_data: bad_aad ).decode(msg) }.to raise_error AES_GCM::Error
        }
        And {
          expect { klass.new( key: secret, auth_data: bad_aad ).decode(msg) }.to raise_error OpenSSL::Cipher::CipherError
        }
      end
    end
  end
end
end
