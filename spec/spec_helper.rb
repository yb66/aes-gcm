require 'rspec/given'
require 'pathname'
$LOAD_PATH.unshift Pathname(__dir__).join("../lib").to_path

require 'simplecov'

SimpleCov.start do
  add_filter "/vendor/"
  add_filter "/vendor.noindex/"
  add_filter "/bin/"
end

RSpec.configure do |conf|
end
