require 'spec_helper'
#require "aes-gcm/msgpack"
require 'securerandom'

require 'aes-gcm'
require 'msgpack'

AES_GCM::GCMable::PACKER = MessagePack

describe "Using serializer/packers should be interchangable" do
  klass = AES_GCM::AES_128_GCM
  bits  = 128
  Given(:key_length) { bits.div 8 }
  context "MessagePack" do
    context 'An encrypted message is decryptable' do
      Given(:secret) { SecureRandom.base64( key_length ) }
      Given(:msg) { klass.new( key: secret ).encode('Hello, World') }
      Given(:plain) { klass.new( key: secret ).decode(msg)}
      Then { plain == 'Hello, World' }
      Then { AES_GCM::GCMable::PACKER == MessagePack }
    end
  end
end